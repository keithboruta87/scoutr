angular.module('starter.services', [])

.factory('Teams', ['fbutil', function(fbutil) {
   return fbutil.syncArray('teams');
 }]);
