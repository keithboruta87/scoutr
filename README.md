
## Team 4405 Scoutr

This is Team 4405's scouting system built for the 2015 FRC season, "Recycle Rush". 

### How to install

1. Install [Node JS](http://nodejs.org/)

2. Install Ionic framework
```bash
$ npm install -g ionic 
```

3. Go into the folder you pulled tis repo to

4. Start the app:
```bash
$ ionic serve
```
and the app will be availible on http://localhost:8100.

## Demo
https://scouter.firebaseapp.com
